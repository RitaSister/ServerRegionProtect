package net.ritasister.listener.protect;

import java.util.*;

import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.*;
import org.bukkit.event.player.*;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.projectiles.*;

import com.sk89q.worldedit.bukkit.*;
import com.sk89q.worldedit.math.*;
import com.sk89q.worldguard.*;
import com.sk89q.worldguard.bukkit.*;
import com.sk89q.worldguard.protection.*;
import com.sk89q.worldguard.protection.managers.*;
import com.sk89q.worldguard.protection.regions.*;

import net.ritasister.srp.SRPApi;
import net.ritasister.srp.ServerRegionProtect;
import net.ritasister.util.UtilPermissions;
import net.ritasister.util.wg.Iwg;
import net.ritasister.util.wg.wg7;

public class RegionProtect implements Listener
{
	private ServerRegionProtect serverRegionProtect;
	private Iwg wg;
	private final List<String> regionEditArgs = Arrays.asList("f", "flag");
	private final List<String> regionEditArgsFlags = Arrays.asList("-f", "-u", "-n", "-g", "-a");

	public RegionProtect(ServerRegionProtect serverRegionProtect)
	{
		this.serverRegionProtect=serverRegionProtect;
		this.wg = this.setupWG();
	}
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
	private void BBEOnReg(final BlockBreakEvent e) 
	{
		final Block b = e.getBlock();
		final Location loc = b.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtectAllow)
			|| SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow))
		{
			e.setCancelled(false);
		}else
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect)) 
		{
			final Player p = e.getPlayer();
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				e.setCancelled(true);
				if (ServerRegionProtect.utilConfig.regionMessageProtect)
				{
					p.sendMessage(ServerRegionProtect.utilConfigMessage.srpMsg);  
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
	private void BPEOnReg(final BlockPlaceEvent e)
	{
		final Block b = e.getBlock();
		final Location loc = b.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtectAllow))
		{
			e.setCancelled(false);
		}else
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect)
				|| SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtectAllow)) 
		{
			final Player p = e.getPlayer();
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				e.setCancelled(true);
				if (ServerRegionProtect.utilConfig.regionMessageProtect)
					p.sendMessage(ServerRegionProtect.utilConfigMessage.srpMsg); 
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void PBEEOnReg(final PlayerBucketEmptyEvent e) 
	{
		final Player p = e.getPlayer();
		final Location clickLoc = e.getBlockClicked().getLocation();
		if(SRPApi.checkStandingRegion(clickLoc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				if (e.getBlockClicked().getType() == Material.LAVA_BUCKET
						&& e.getBlockClicked().getType() == Material.WATER_BUCKET
						&& e.getBlockClicked().getType() == Material.BUCKET)
				{
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void EDBEEOnReg(final EntityDamageByEntityEvent e) 
	{
		final Entity entity = e.getEntity();
		final Entity damager = e.getDamager();
		final Location loc = entity.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(damager, UtilPermissions.serverRegionProtect, null))return;
			{
				if (entity instanceof ArmorStand 
						|| entity instanceof ItemFrame 
						|| entity instanceof Painting
						|| entity instanceof EnderCrystal)
				{
					if (damager instanceof Player)
					{
						e.setCancelled(true);
					}else if (damager instanceof Arrow) 
					{
						ProjectileSource shooter = ((Arrow) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof SpectralArrow) 
					{
						ProjectileSource shooter = ((SpectralArrow) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Snowball) 
					{
						ProjectileSource shooter = ((Snowball) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Egg) 
					{
						ProjectileSource shooter = ((Egg) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof EnderPearl) 
					{
						ProjectileSource shooter = ((EnderPearl) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Trident) 
					{
						ProjectileSource shooter = ((Trident) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void PIEEOnReg(final PlayerInteractEntityEvent e)
	{
		final Player p = e.getPlayer();
		final Location clickLoc = e.getRightClicked().getLocation();
		if(SRPApi.checkStandingRegion(clickLoc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				if(e.getRightClicked().getType() == EntityType.ITEM_FRAME
						&& this.wg.wg(p.getWorld(), p.getLocation(), false))
				{
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void stand(final PlayerArmorStandManipulateEvent e)
	{
		final Player p = e.getPlayer();
		final Location clickLoc = e.getRightClicked().getLocation();
		if(SRPApi.checkStandingRegion(clickLoc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				if(e.getRightClicked().getType() == EntityType.ARMOR_STAND 
						&& this.wg.wg(p.getWorld(), clickLoc, false))
				{
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void SGEOnReg(final StructureGrowEvent e) 
	{
		if (this.wg.wg(e.getWorld(), e.getLocation(), false)) 
		{
			e.setCancelled(true);
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void PIEOnReg(final PlayerInteractEvent e)
	{
		if (e.getItem() != null) 
		{
			Player p = e.getPlayer();
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				for(String spawnEntityType : ServerRegionProtect.utilConfig.spawnEntityType)
				{
					if(e.getItem().getType() == Material.getMaterial(spawnEntityType.toUpperCase())
						&& e.getClickedBlock() != null && this.wg.wg(p.getWorld(), e.getClickedBlock().getLocation(), false))
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void HBBEEOnReg(final HangingBreakByEntityEvent e)
	{
		final Entity entity = e.getEntity();
		final Entity damager = e.getRemover();
		final Location loc = entity.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(damager, UtilPermissions.serverRegionProtect, null))return;
			{
				if (e.getRemover().getType() == EntityType.PLAYER) 
				{
					e.setCancelled(true);
				}
				if (entity instanceof ItemFrame 
					|| entity instanceof Painting) 
				{
					if (damager instanceof Player) 
					{
						e.setCancelled(true);
					}else if (damager instanceof Arrow) 
					{
						ProjectileSource shooter = ((Arrow) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof SpectralArrow) 
					{
						ProjectileSource shooter = ((SpectralArrow) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Snowball) 
					{
						ProjectileSource shooter = ((Snowball) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Egg) 
					{
						ProjectileSource shooter = ((Egg) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof EnderPearl) 
					{
						ProjectileSource shooter = ((EnderPearl) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}else if (damager instanceof Trident) 
					{
						ProjectileSource shooter = ((Trident) damager).getShooter();
						if (shooter instanceof Player) 
						{
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void PHEOnReg(final HangingPlaceEvent e)
	{
		final Entity entity = e.getEntity();
		final Player p = (Player) e.getPlayer();
		final Location loc = entity.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
			{
				if (p.getInventory().getItemInMainHand().getType() == Material.ITEM_FRAME
						|| p.getInventory().getItemInMainHand().getType() == Material.PAINTING) 
				{
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler(priority=EventPriority.NORMAL, ignoreCancelled = false)
	private void EEEOnReg(final EntityExplodeEvent e) 
	{
		final Entity entity = e.getEntity();
		final Location $loc = entity.getLocation();
		if(SRPApi.checkStandingRegion($loc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(e.getEntityType() == EntityType.PRIMED_TNT 
					|| e.getEntityType() == EntityType.ENDER_CRYSTAL 
					|| e.getEntityType() == EntityType.MINECART_TNT) 
			{
				e.blockList().clear();
			}
		}
	}
	@EventHandler(priority=EventPriority.NORMAL, ignoreCancelled = false)
	public void BEEonReg(final BlockExplodeEvent e) 
	{
		final Block b = e.getBlock();
		final Location loc = b.getLocation();
		if(SRPApi.checkStandingRegion(loc, ServerRegionProtect.utilConfig.regionProtect))
		{
			if(b.getType() == null)return;
			{
				if(b.getType() == Material.RESPAWN_ANCHOR)return;
				{
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void wg(final PlayerCommandPreprocessEvent e) {
		final Player p = e.getPlayer();
		final String[] s = e.getMessage().toLowerCase().split(" ");
		if(SRPApi.isAuthListenerPermission(p, UtilPermissions.serverRegionProtect, null))return;
		{
			if (this.cmdwe(s[0]) && !this.wg.checkIntersection(p)) {
				e.setCancelled(true);
			}
			if (this.cmdweC(s[0]) && !this.wg.checkCIntersection(p, s)) {
				e.setCancelled(true);
			}
			if (this.cmdweP(s[0]) && !this.wg.checkPIntersection(p, s)) {
				e.setCancelled(true);
			}
			if (this.cmdweS(s[0]) && !this.wg.checkSIntersection(p, s)) {
				e.setCancelled(true);
			}
			if (this.cmdweU(s[0]) && !this.wg.checkUIntersection(p, s)) {
				e.setCancelled(true);
			}
			if (this.cmdweCP(s[0])) {
				e.setMessage(e.getMessage().replace("-o", ""));
				if (!this.wg.checkCPIntersection(p, s)) {
					e.setCancelled(true);
				}
			}
			if ((s[0].equalsIgnoreCase("/rg") 
					|| s[0].equalsIgnoreCase("/region") 
					|| s[0].equalsIgnoreCase("/regions") 
					|| s[0].equalsIgnoreCase("/worldguard:rg") 
					|| s[0].equalsIgnoreCase("/worldguard:region") 
					|| s[0].equalsIgnoreCase("/worldguard:regions")) && s.length > 2) 
			{
				for (final String list : ServerRegionProtect.utilConfig.regionProtect) 
				{
					if (list.equalsIgnoreCase(s[2])) 
					{
						e.setCancelled(true);
					}
				}
				for (final String list : ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow)
				{
					if (list.equalsIgnoreCase(s[2])) 
					{
						e.setCancelled(true);
					}
				}
				if (s.length > 3 && this.regionEditArgs.contains(s[2].toLowerCase()) 
						|| s.length > 3 && this.regionEditArgsFlags.contains(s[2].toLowerCase()))
				{
					for (final String list : ServerRegionProtect.utilConfig.regionProtect) 
					{
						if (list.equalsIgnoreCase(s[3])) 
						{
							e.setCancelled(true);
						}
					}
					for (final String list : ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow) 
					{
						if (list.equalsIgnoreCase(s[3])) 
						{
							e.setCancelled(true);
						}
					}
				}
				if(s.length > 4 && s[2].equalsIgnoreCase("-w")
						|| s.length > 4 && this.regionEditArgsFlags.contains(s[2].toLowerCase()))
				{
					for (final String list : ServerRegionProtect.utilConfig.regionProtect) 
					{
						if (list.equalsIgnoreCase(s[4])) 
						{
							e.setCancelled(true);
						}
					}
					for (final String list : ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow) 
					{
						if (list.equalsIgnoreCase(s[4])) 
						{
							e.setCancelled(true);
						}
					}
				}
				if (s.length > 5 && s[3].equalsIgnoreCase("-w")
						|| s.length > 5 && this.regionEditArgs.contains(s[4].toLowerCase())
						|| s.length > 5 && this.regionEditArgsFlags.contains(s[4].toLowerCase())) 
				{
					for (final String list : ServerRegionProtect.utilConfig.regionProtect) 
					{
						if (list.equalsIgnoreCase(s[5])) 
						{
							e.setCancelled(true);
						}
					}
					for (final String list : ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow) 
					{
						if (list.equalsIgnoreCase(s[5])) 
						{
							e.setCancelled(true);
						}
					}
				}
				if (s.length > 6 && s[4].equalsIgnoreCase("-w")
						|| s.length > 6 && s[4].equalsIgnoreCase("-h")
						|| s.length > 6 && s[4].equalsIgnoreCase("-a")
						|| s.length > 6 && this.regionEditArgs.contains(s[5].toLowerCase())
						|| s.length > 6 && this.regionEditArgsFlags.contains(s[5].toLowerCase())) 
				{
					for (final String list : ServerRegionProtect.utilConfig.regionProtect) 
					{
						if (list.equalsIgnoreCase(s[6])) 
						{
							e.setCancelled(true);
						}
					}
					for (final String list : ServerRegionProtect.utilConfig.regionProtectOnlyBreakAllow) 
					{
						if (list.equalsIgnoreCase(s[6])) 
						{
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
	private Iwg setupWG() 
	{
		try{
			Class.forName("com.sk89q.worldedit.math.BlockVector3");
			return new wg7(this.serverRegionProtect);
		}catch(ClassNotFoundException ex){
		}
		return wg;
	}
	private boolean cmdwe(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.CmdsWE) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
	private boolean cmdweC(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.cmdweC) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
	private boolean cmdweP(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.cmdweP) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
	private boolean cmdweS(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.cmdweS) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
	private boolean cmdweU(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.cmdweU) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
	private boolean cmdweCP(String s) 
	{
		s = s.replace("worldedit:", "");
		for(String tmp : ServerRegionProtect.utilConfig.cmdweCP) 
		{
			if (tmp.equalsIgnoreCase(s.toLowerCase())) 
			{
				return true;
			}
		}
		return false;
	}
}